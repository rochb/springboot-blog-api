package me.roch.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NullArticleException extends RuntimeException {

    public NullArticleException(String message) {
        super(message);
    }
}
