package me.roch.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@ToString
@AllArgsConstructor
@Entity
@NoArgsConstructor
public class Article {

    @Id
    @GeneratedValue
    private int id;

    @Length(min = 3, message = "Title cannot be shorter than 3 characters!")
    private String title;
    private String content;
    private String author;
}
