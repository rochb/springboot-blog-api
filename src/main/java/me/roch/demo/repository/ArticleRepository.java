package me.roch.demo.repository;

import me.roch.demo.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {

}
