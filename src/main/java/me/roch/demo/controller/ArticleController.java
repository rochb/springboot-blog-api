package me.roch.demo.controller;

import me.roch.demo.exceptions.NullArticleException;
import me.roch.demo.model.Article;
import me.roch.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RestController
public class ArticleController {

    @Autowired
    private ArticleRepository repository;

    @GetMapping("/articles")
    public List<Article> getArticles() {
        return repository.findAll();
    }

    @GetMapping("/articles/{id}")
    public Article getArticle(@PathVariable("id") int id) throws NullArticleException {
        return repository.findById(id)
                .orElseThrow(() -> new NullArticleException(String.format("Unable to find article with id '%d'!", id)));
    }

    @PostMapping("/articles")
    public ResponseEntity<Void> addArticle(@Valid @RequestBody Article article) {
        repository.save(article);
        if (repository.findById(article.getId()).isEmpty())
            return ResponseEntity.noContent().build();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(article.getId())
                .toUri();
        return ResponseEntity.created(location)
                .build();
    }

}
