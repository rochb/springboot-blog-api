![Alt text](https://www.vincenzoracca.com/images/spring.png)

SpringBoot Blog API
------------------


A simple rest blog api developed with SpringBoot.

Table of Contents
------------------
1. About The Project
2. Getting Started
3. Improvements
4. Contact

## About The Project
I decided to learn how to use Spring Boot and Spring to make micro-services and web application. So this was my very first projet using those tools. I discovered how to handle GET/PUT/POST/... requests and how to answer them with an appropriate HTTP code. I also learn how to use Jpa to store entities to a database (in this case I used H2).

## Getting Started

To get a local copy up and running follow these simple steps.

### Installation

1. Clone the repo
   ```sh
   git clone https://bitbucket.org/rochb/springboot-blog-api
   ```
2. Run the server
   ```maven
   mvn clean spring-boot:run
   ```

## Improvements

Here is a list of things that can be greatly improved:

- Actually every feature can be improved, but I decided to keep it really simple.
- More seriously, I think the data management could be improved (I currently don't know how to do it, hopefully I'll learn that later).

## Contact

Roch Blondiaux
:earth_americas: [www.roch-blondiaux.com](https://roch-blondiaux.com)
:email: [contact@roch-blondiaux.com](mailto:contact@roch-blondiaux.com)
